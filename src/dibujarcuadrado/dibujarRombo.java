
package dibujarcuadrado;

import java.util.Scanner;

public class dibujarRombo {
         
    private static Scanner sc;
     
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        boolean numeroCorrecto = false;
        int n; 
        do {
            System.out.print("Introduce número impar: ");
            n = sc.nextInt();
            if(n>0 && n%2!=0){
                numeroCorrecto = true;
            }
        } while (!numeroCorrecto);
         
        System.out.println("");
         
        //ROMBO RELLENO
        int numFilas = n/2 + 1;
                 
        for(int altura=1;altura<=numFilas;altura++){
            for(int blancos=1;blancos<=numFilas-altura;blancos++){
                System.out.print("  ");
            }
            for(int asteriscos=1;asteriscos<=(2*altura)-1;asteriscos++){
                System.out.print("* ");
            }
            System.out.println("");         
        }
         
        numFilas--;
         
        for(int altura=1;altura<=numFilas;altura++){
            for(int blancos=1;blancos<=altura;blancos++){
                System.out.print("  ");
            }
            for(int asteriscos=1;asteriscos<=(numFilas-altura)*2 +1;asteriscos++){
                System.out.print("* ");
            }
            System.out.println();
        }
        //------------------------------
        
        //ROMBO CONTORNO
        char r [][] = new char[n][n];
        
        for(int i=0; i<n; i++){
            for (int j=0;j<n;j++){
                r[i][j]= ' ';
            }
        }
        int m = n/2;
        for (int i = 0; i <= m ; i++) {
            r[i][m-i] = '*';
            r[i][m+i] = '*';
            r[n-i-1][m-i] = '*';
            r[n-i-1][m+i] = '*';
        }
        for(int i=0; i<n; i++){
            for (int j=0;j<n;j++){
                System.out.print(" "+r[i][j]);
            }
            System.out.println();
        }
    }
}
